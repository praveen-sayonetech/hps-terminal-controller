Steps:

* Install the maven dependencies.

* Build the project. (you will get a single executable jar file in your target folder)

* Run the jar file. (you'll only see a tray icon)

* Set the pin-pad private IP using 'set terminal IP' option from tray.

* make sure the local server is running by going to http://localhost:9000.

Available endpoints:

Display server status, processed by RootHandler
http://localhost:9000

Display header information, processed by EchoHeaderHandler.
http://localhost:9000/echoHeader

GET requests processed by EchoGetHandler
http://localhost:9000/echoGet

POST requests processed by EchoPostHandler
http://localhost:9000/echoPost

For initiating a credit sale on the pinpad, pass the below GET request to echoGet url.
http://localhost:9000/echoGet/?type=card-pay&amount=10

