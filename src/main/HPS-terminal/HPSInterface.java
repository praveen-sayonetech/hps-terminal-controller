import com.global.api.terminals.TerminalResponse;
import java.util.Map;

public interface HPSInterface {
//    common interface for various HPS operations
    public TerminalResponse processCard(Map<String, Object> parameters);
}