import com.global.api.terminals.TerminalResponse;
import com.sun.net.httpserver.Headers;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import org.json.JSONObject;

import java.io.*;
import java.net.URI;
import java.net.URLDecoder;
import java.util.*;

class Handlers {
	public static class RootHandler implements HttpHandler {
//		url: http://localhost:9000
		public void handle(HttpExchange he) throws IOException {
			String response = "<h1 style='color: cadetblue; text-align: center'>Server is running.</h1>" +
					"<p style='text-align: center'>Port: " + Main.port + "</p>";
			he.sendResponseHeaders(200, response.length());
			OutputStream os = he.getResponseBody();
			os.write(response.getBytes());
			os.close();
		}
	}

	public static class EchoHeaderHandler implements HttpHandler {
//		url: http://localhost:9000/echoHeader
		public void handle(HttpExchange he) throws IOException {
			Headers headers = he.getRequestHeaders();
			Set<Map.Entry<String, List<String>>> entries = headers.entrySet();
			StringBuilder response = new StringBuilder();
			for (Map.Entry<String, List<String>> entry : entries)
				response.append(entry.toString()).append("\n");
			he.sendResponseHeaders(200, response.length());
			OutputStream os = he.getResponseBody();
			os.write(response.toString().getBytes());
			os.close();
		}
	}

	public static class EchoGetHandler implements HttpHandler {
//		url: http://localhost:9000/echoGet
		HPSInterface hpsAction;
		public void handle(HttpExchange he) throws IOException {
//			parsing request url
			Map<String, Object> parameters = new HashMap<String, Object>();
			URI requestedUri = he.getRequestURI();
			String query = requestedUri.getRawQuery();
			parseQuery(query, parameters);

//			finding the operation requested and processing it
			hpsAction = getRequestType(parameters);
			TerminalResponse hps_response = hpsAction.processCard(parameters);

//			sending response back
			JSONObject response = new JSONObject();
			if (hps_response == null) {
//				payment error
				response.put("success", false);
			}
			else{
//				payment success
				response.put("success", true);
				response.put("transaction_id", hps_response.getTransactionId());
				response.put("card_type", hps_response.getApplicationLabel());
				response.put("masked_number", hps_response.getMaskedCardNumber());
				response.put("response_text", hps_response.getResponseText());
				response.put("reference_number", hps_response.getApprovalCode());
				response.put("authorization_code", hps_response.getAuthorizationCode());
				response.put("transaction_amount", hps_response.getTransactionAmount());
				response.put("message", hps_response.toString());
			}
			for (String key : parameters.keySet())
				response.put(key, parameters.get(key));
			he.getResponseHeaders().add("Access-Control-Allow-Origin", "*");
			he.sendResponseHeaders(200, response.toString().length());
			OutputStream os = he.getResponseBody();
			os.write(response.toString().getBytes());
			os.close();
		}

		private HPSInterface getRequestType(Map<String, Object> parameters) {
//			finds the type of operation requested based on the 'type' parameter
			String action = parameters.get("type").toString();
			if ("card-pay".equals(action)) {
				return new Operations.CardPay();
			}
//			default is CardPay
			return new Operations.CardPay();
		}
	}

	public static class EchoPostHandler implements HttpHandler {
//		url: http://localhost:9000/echoPost
		public void handle(HttpExchange he) throws IOException {
			// parse request
			Map<String, Object> parameters = new HashMap<String, Object>();
			InputStreamReader isr = new InputStreamReader(he.getRequestBody(), "utf-8");
			BufferedReader br = new BufferedReader(isr);
			String query = br.readLine();
			parseQuery(query, parameters);
			// send response
			StringBuilder response = new StringBuilder();
			for (String key : parameters.keySet())
				response.append(key).append(" = ").append(parameters.get(key)).append("\n");
			he.sendResponseHeaders(200, response.length());
			OutputStream os = he.getResponseBody();
			os.write(response.toString().getBytes());
			os.close();

		}
	}

	@SuppressWarnings("unchecked")
	private static void parseQuery(String query, Map<String, Object> parameters) throws UnsupportedEncodingException {
//		parses the url parameters in the query string and adds them to 'parameters' variable
		if (query != null) {
			String[] pairs = query.split("[&]");

			for (String pair : pairs) {
				String[] param = pair.split("[=]");

				String key = null;
				String value = null;
				if (param.length > 0) {
					key = URLDecoder.decode(param[0], System.getProperty("file.encoding"));
				}

				if (param.length > 1) {
					value = URLDecoder.decode(param[1], System.getProperty("file.encoding"));
				}

				if (parameters.containsKey(key)) {
					Object obj = parameters.get(key);
					if (obj instanceof List<?>) {
						List<String> values = (List<String>) obj;
						values.add(value);
					} else if (obj instanceof String) {
						List<String> values = new ArrayList<String>();
						values.add((String) obj);
						values.add(value);
						parameters.put(key, values);
					}
				} else {
					parameters.put(key, value);
				}
			}
		}
	}
}
