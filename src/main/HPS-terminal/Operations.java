import com.global.api.ServicesConfig;
import com.global.api.entities.enums.ConnectionModes;
import com.global.api.entities.enums.DeviceType;
import com.global.api.services.DeviceService;
import com.global.api.terminals.ConnectionConfig;
import com.global.api.terminals.TerminalResponse;
import com.global.api.terminals.abstractions.IDeviceInterface;

import java.awt.*;
import java.math.BigDecimal;
import java.util.Map;

class Operations {
    private static IDeviceInterface paxDevice;

    static void initialize_device(String ipAddress){
        try {
//            getting the terminal_ip preference value
            if (ipAddress == null) {
                ipAddress = TrayIconTerminal.prefs.get("terminal_ip", null);
                if (ipAddress == null) {
                    TrayIconTerminal.trayIcon.displayMessage("Error", "Please set the terminal IP",
                            TrayIcon.MessageType.ERROR);
                    paxDevice = null;
                    return;
                }
            }
//            setting up the connection using TCP/IP
            ConnectionConfig deviceConfig = new ConnectionConfig();
            deviceConfig.setDeviceType(DeviceType.PAX_S300);
            deviceConfig.setConnectionMode(ConnectionModes.TCP_IP);
            deviceConfig.setIpAddress(ipAddress);         // private IP of the machine
            deviceConfig.setPort(10009);                  // port of the machine
            deviceConfig.setTimeout(30000);
            paxDevice = DeviceService.create(deviceConfig);
            System.out.println(ipAddress+": Device initialized.... ");
        } catch (Exception e) {
            e.printStackTrace();
            TrayIconTerminal.trayIcon.displayMessage("Terminal Error", "Please make sure the terminal is " +
                            "connected to your network and set its IP correctly.\n"+e.getMessage(),
                    TrayIcon.MessageType.ERROR);
            paxDevice = null;
        }
    }

    public static class CardPay implements HPSInterface {
//        this class is used to initiate a credit card payment with an specified amount
        @Override
        public TerminalResponse processCard(Map<String, Object> parameters) {
            try {
                if(paxDevice == null){
                    initialize_device(null);
                    if (paxDevice == null) {
                        return null;
                    }
                }
//                Charging the card
                String amountString = parameters.get("amount").toString();
                BigDecimal amount = new BigDecimal(amountString);
                TerminalResponse sale_resp = paxDevice.creditSale(1, amount).execute();
                System.out.println("Response received.... \n"+sale_resp.toString());

                System.out.println("getApplicationCryptogram\n"+sale_resp.getApplicationCryptogram());
                System.out.println("getApplicationId\n"+sale_resp.getApplicationId());
                System.out.println("getApplicationLabel\n"+sale_resp.getApplicationLabel());
                System.out.println("getApplicationPreferredName\n"+sale_resp.getApplicationPreferredName());
                System.out.println("getApprovalCode\n"+sale_resp.getApprovalCode());
                System.out.println("getAuthorizationCode\n"+sale_resp.getAuthorizationCode());
                System.out.println("getAvsResponseCode\n"+sale_resp.getAvsResponseCode());
                System.out.println("getAvsResponseText\n"+sale_resp.getAvsResponseText());
                System.out.println("getCardBIN\n"+sale_resp.getCardBIN());
                System.out.println("getCardHolderName\n"+sale_resp.getCardHolderName());
                System.out.println("getCommand\n"+sale_resp.getCommand());
                System.out.println("getTaxExemptId\n"+sale_resp.getTaxExemptId());
                System.out.println("getCvvResponseCode\n"+sale_resp.getCvvResponseCode());
                System.out.println("getCvvResponseText\n"+sale_resp.getCvvResponseText());
                System.out.println("getDeviceResponseCode\n"+sale_resp.getDeviceResponseCode());
                System.out.println("getDeviceResponseText\n"+sale_resp.getDeviceResponseText());
                System.out.println("getExpirationDate\n"+sale_resp.getExpirationDate());
                System.out.println("getMaskedCardNumber\n"+sale_resp.getMaskedCardNumber());
                System.out.println("getPaymentType\n"+sale_resp.getPaymentType());
                System.out.println("getResponseCode\n"+sale_resp.getResponseCode());
                System.out.println("getResponseText\n"+sale_resp.getResponseText());
                System.out.println("getStatus\n"+sale_resp.getStatus());
                System.out.println("getVersion\n"+sale_resp.getVersion());
                System.out.println("getTicketNumber\n"+sale_resp.getTicketNumber());
                System.out.println("getToken\n"+sale_resp.getToken());
                System.out.println("getEntryMethod\n"+sale_resp.getEntryMethod());
                System.out.println("getSignatureStatus\n"+sale_resp.getSignatureStatus());
                System.out.println("getTransactionId\n"+sale_resp.getTransactionId());
                System.out.println("getTerminalRefNumber\n"+sale_resp.getTerminalRefNumber());
                System.out.println("getTransactionType\n"+sale_resp.getTransactionType());
                System.out.println("getTransactionAmount\n"+sale_resp.getTransactionAmount());
                System.out.println("getAmountDue\n"+sale_resp.getAmountDue());
                System.out.println("getTipAmount\n"+sale_resp.getTipAmount());
                System.out.println("getCashBackAmount\n"+sale_resp.getCashBackAmount());
                System.out.println("getCustomerVerificationMethod\n"+sale_resp.getCustomerVerificationMethod());
                System.out.println("getTerminalVerificationResults\n"+sale_resp.getTerminalVerificationResults());
                System.out.println("getBalanceAmount\n"+sale_resp.getBalanceAmount());

                if(sale_resp.getDeviceResponseText().equals("OK") && sale_resp.getResponseText().equals("APPROVAL")){
                    return sale_resp;
                }else{
                    TrayIconTerminal.trayIcon.displayMessage("Payment Error",
                            "HPS payment error.\n"+sale_resp.getResponseText(),
                            TrayIcon.MessageType.ERROR);
                    return null;
                }
            } catch (Exception e) {
                e.printStackTrace();
                TrayIconTerminal.trayIcon.displayMessage("Payment Error",
                        "HPS payment gateway error.\n"+e.getMessage(),
                        TrayIcon.MessageType.ERROR);
                return null;
            }
        }
    }
}