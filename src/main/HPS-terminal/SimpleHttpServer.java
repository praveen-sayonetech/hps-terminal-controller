import com.sun.net.httpserver.HttpServer;

import java.awt.*;
import java.io.IOException;
import java.net.InetSocketAddress;

class SimpleHttpServer {
	private static HttpServer server;

	static void Start(int port) {
//		creating and initializing the tray icon
		new TrayIconTerminal();
		try {
//			starting the HTTP server and adding the HTTP method handlers
			server = HttpServer.create(new InetSocketAddress(port), 0);
			System.out.println("HPS server started at http://127.0.0.1:" + port);
			System.out.println("Listening for incoming connections at endpoints [/, /echoHeader, /echoGet, /echoPost]");
			server.createContext("/", new Handlers.RootHandler());
			server.createContext("/echoHeader", new Handlers.EchoHeaderHandler());
			server.createContext("/echoGet", new Handlers.EchoGetHandler());
			server.createContext("/echoPost", new Handlers.EchoPostHandler());
			server.setExecutor(null);
			server.start();
		} catch (IOException e) {
			e.printStackTrace();
			TrayIconTerminal.trayIcon.displayMessage("Server Error", "Couldn\'t start the server."
							+e.getMessage(),
					TrayIcon.MessageType.ERROR);
		}
	}

	static void Stop() {
//		stop the HTTP server
		server.stop(0);
		System.out.println("server stopped");
	}
}
