import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.net.URL;
import java.util.prefs.Preferences;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

class TrayIconTerminal {

    static TrayIcon trayIcon;
    static Preferences prefs;

    TrayIconTerminal(){
//        initilizing the tray icon
        setTrayIcon();

//        setting the look and feel to system
        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        }
        catch (UnsupportedLookAndFeelException e) {
            e.printStackTrace();
        }
        catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        catch (InstantiationException e) {
            e.printStackTrace();
        }
        catch (IllegalAccessException e) {
            e.printStackTrace();
        }

//		checking if terminal_ip exist in preferences
        prefs = Preferences.userRoot().node(this.getClass().getName());
        String ipAddress = prefs.get("terminal_ip", null);
        if (ipAddress == null) {
            trayIcon.displayMessage("Error", "Please set the terminal IP",
                    TrayIcon.MessageType.ERROR);
        }else{
            System.out.println("Using terminal IP: " + ipAddress);
        }
    }

    private void setTrayIcon(){
        if(!SystemTray.isSupported()){
            System.exit(0);
            return;
        }

        final SystemTray tray = SystemTray.getSystemTray();
        URL imageUrl = TrayIconTerminal.class.getResource("/Images/pin-pad.png");
        trayIcon = new TrayIcon((new ImageIcon(imageUrl, "Tray Icon")).getImage());
        trayIcon.setToolTip("Controller server for pax terminals");

        final PopupMenu popup = new PopupMenu();
        MenuItem setIP = new MenuItem("Set terminal IP");
        MenuItem exit = new MenuItem("Exit");
        popup.add(setIP);
        popup.add(exit);
        trayIcon.setPopupMenu(popup);

//        adding menuItem listeners (setIP and exit)
        setIP.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                boolean valid = false;
                String terminalIP = null;
                JFrame frame = new JFrame("Terminal IP Dialog");
                while(!valid){
                    UIManager.put("OptionPane.okButtonText", "Connect");
                    terminalIP = JOptionPane.showInputDialog(frame,
                            "Please enter your terminal IP");
                    if (terminalIP == null) break;
                    valid = this.validateIP(terminalIP);
                    if (valid) {
//                        setting/updating the terminal_ip preferences value and initializing the device
                        prefs.put("terminal_ip", terminalIP);
                        Operations.initialize_device(terminalIP);
                    }
                }
            }

            private boolean validateIP(String terminalIP) {
//                validating the IP address entered
                if(terminalIP == null){
                    return false;
                }
//                regex for IP addresses
                Pattern p = Pattern.compile("^((25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$");
                Matcher m = p.matcher(terminalIP);
                if (!m.find()){
                    UIManager.put("OptionPane.okButtonText", "Retry");
                    JOptionPane.showMessageDialog(null, "Please enter a valid IP", "Error", JOptionPane.ERROR_MESSAGE);
                    return false;
                }
                return true;
            }
        });

        exit.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                tray.remove(trayIcon);
                SimpleHttpServer.Stop();
                System.exit(0);
            }
        });

//        finally adding the trayIcon to the system tray
        try{
            tray.add(trayIcon);
        } catch (AWTException e) {
            e.printStackTrace();
        }
    }
}
